package com.agiletestingalliance;

public class MinMax {

	public int function(int first, int second) {
		if (second > first){
			return second;
		}
		else {
			return first; 
		}
	}

	
	public String barStr(String string) {
		if (string == null){
			return null;
		}

		if ("".equals(string)){
			return "";
		}

		// These above are useless since we only ever ret string, should return $string or "null" or "empty"
		return string;
	}
	

}
