package com.agiletestingalliance;

public class TestClass {

  private String string = null;
  
  public TestClass(String string) {
    this.string = string;

  }

  public String getString(){
    System.out.println("String value =" + string);
    return string;
  }
}
