package com.agiletestingalliance;

import  static org.junit.Assert.*;
import org.junit.Test;

public class TestClassTest {

  @Test
  public void testGString(){

    final String test = new TestClass("abc123").getString();
    assertEquals("Get string equality", test, "abc123");

  }
}
