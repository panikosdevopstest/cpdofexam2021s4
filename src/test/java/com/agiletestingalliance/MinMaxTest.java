package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {

	@Test
	public void testFunctionFirst(){

		final int value = new MinMax().function(2,1);
		assertEquals("First", value , 2);

	}
	@Test
	public void testFunctionSecond(){

		final int value = new MinMax().function(1,2);
		assertEquals("Second", value , 2);

	}

	@Test
	public void testFunctionEmpty(){

		final String string  = new MinMax().barStr("");
		assertEquals("Empty", string , "");

	}

	@Test
	public void testFunction(){

		final String string  = new MinMax().barStr("test");
		assertEquals("Test str", string , "test");

	}

	@Test
	public void testFunctionNull(){

		final String string  = new MinMax().barStr(null);
		assertEquals("Null", string , null);

	}



}
